module.exports = function () {

    var faker = require("faker");
    var _ = require("lodash");
    return {
        vehicle: _.times(100, function (n) {

            return {

                id: n,                
                manufacturer: faker.vehicle.manufacturer(),
                colour: faker.vehicle.color()

            }


        })


    }

}